import 'package:flutter/material.dart';

const defaultPrimaryColor = Color(0xFF6F35A5);
const defaultPrimaryLightColor = Color(0xFFF1E6FF);
