import 'package:authorization/components/text_field_container.dart';
import 'package:authorization/static_values/constants.dart';
import 'package:flutter/material.dart';

class RoundedFieldPassword extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedFieldPassword({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: defaultPrimaryColor,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: defaultPrimaryColor,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
