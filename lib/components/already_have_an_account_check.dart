import 'package:authorization/static_values/constants.dart';
import 'package:flutter/material.dart';

class AlredyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;
  const AlredyHaveAnAccountCheck({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          login ? "Don't have an account ? " : "Already have an account ? ",
          style: TextStyle(color: defaultPrimaryColor),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "Sign Up" : "Sign In",
            style: TextStyle(
                color: defaultPrimaryColor, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
